use std::env;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

fn main() -> Result<(), std::io::Error> {
	let args: Vec<String> = env::args().collect();
	assert!(args.len()==2, "Usage: prproofer path_to_proof_file.proof");

	//Open file
	let f = File::open(&args[1]).expect("Failed to open proof file");
	let mut r = BufReader::new(&f);

	//Read header from file
	let mut index = 0;
	let headcheck = ["PRP PROOF", "VERSION=", "HASHSIZE=", "POWER=", "NUMBER=M"];
	let mut headvals: [u32;5] = [0;5];
	while index<5 {
		let mut line = String::new();
		r.read_line(&mut line).expect("Failed to read line from header");
		if !&line.starts_with(headcheck[index]) {//Field presence check
			println!("Field presence check: Fail. Expected {}, received {}", headcheck[index], &line);
			std::process::exit(1);
		}
		if index>0 {
			headvals[index]=line[headcheck[index].len()..].trim().parse::<u32>().expect("Failed to parse integer");
		}
		index+=1;
	}
	println!("Field presence check: Pass");

	//Range checks
	assert!(headvals[1]==1, "Field range check: Fail. VERSION expected to be 1");
	assert!(headvals[2]==64, "Field range check: Fail. HASHSIZE expected to be 64");
	assert!(headvals[3]>7 && headvals[3]<11, "Field range check: Fail. POWER expected to be 8, 9 or 10");
	println!("Field range check: Pass");

	//Read residues from file
	let reslen = (headvals[4]/8)+1;
	let mut v = vec![];	
	r.read_to_end(&mut v).expect("Read failed");
	assert_eq!(reslen*(headvals[3]+1), v.len() as u32, "Filesize check: Fail");
	println!("Filesize check: Pass");

	//padding checks
	let padlen = 8-(headvals[4]%8);
	let mut index = 0;
	while index<headvals[3]+1 {
		assert!( v[(((index+1)*reslen)-1) as usize]>>(8-padlen) == 0, "Zeroed padding check: Fail");
		index+=1;
	}
	println!("Zeroed padding check: Pass");
	
	let topk=( (headvals[4]/2_u32.pow(headvals[3])) +1) *2_u32.pow(headvals[3]);
	println!("version={}, hashsize={}, power={}, number=M{}, topk={}", headvals[1], headvals[2], headvals[3], headvals[4], topk);
	Ok(())
}
